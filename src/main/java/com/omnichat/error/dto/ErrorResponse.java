package com.omnichat.error.dto;

import java.util.List;

public class ErrorResponse {
  private String code;
  private String message;
  private List<Detail> details;

  public static class Detail {
    private String code;
    private String message;
    private String field;
    private String value;
  }
}
