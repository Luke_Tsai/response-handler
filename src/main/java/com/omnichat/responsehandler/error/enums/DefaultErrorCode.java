package com.omnichat.responsehandler.error.enums;

import com.omnichat.responsehandler.error.dto.ErrorCode;

public class DefaultErrorCode {
  private static final String SYSTEM_CATEGORY = "System";
  public static final ErrorCode SERVER_ERROR =
      new ErrorCode(SYSTEM_CATEGORY, "ServerError", "Internal server error");
  public static final ErrorCode VALIDATION_ERROR =
      new ErrorCode(SYSTEM_CATEGORY, "ValidationError", "Fields validation error");
  public static final ErrorCode NOT_FOUND =
      new ErrorCode(SYSTEM_CATEGORY, "NotFound", "Target resource not found");
  public static final ErrorCode AUTHENTICATION_ERROR =
      new ErrorCode(SYSTEM_CATEGORY, "AuthenticationError", "Not authenticate");
  public static final ErrorCode AUTHORIZATION_ERROR =
      new ErrorCode(SYSTEM_CATEGORY, "AuthorizationError", "Unauthorized");
  public static final ErrorCode BAD_REQUEST =
      new ErrorCode(SYSTEM_CATEGORY, "BadRequest", "The request cannot be processed");

  private DefaultErrorCode() {}
}
