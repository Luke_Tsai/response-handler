package com.omnichat.responsehandler.error.handler;

import com.omnichat.common.enums.system.LogKey;
import com.omnichat.responsehandler.error.dto.ErrorResponse;
import com.omnichat.responsehandler.error.enums.DefaultErrorCode;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletResponse;

@RestController
public class GlobalHttpErrorHandler implements ErrorController {

  @RequestMapping(path = "/error")
  public ErrorResponse error(HttpServletResponse response) {

    String traceId = "";
    var attributes = RequestContextHolder.getRequestAttributes();
    if (attributes != null) {
      traceId = (String) attributes.getAttribute(LogKey.TRACE_ID, RequestAttributes.SCOPE_REQUEST);
    }

    int status = response.getStatus();
    if (status == HttpStatus.NOT_FOUND.value()) {
      return new ErrorResponse(DefaultErrorCode.NOT_FOUND, traceId);
    }

    if (String.valueOf(status).startsWith("4")) {
      return new ErrorResponse(DefaultErrorCode.BAD_REQUEST, traceId);
    }

    return new ErrorResponse(DefaultErrorCode.SERVER_ERROR, traceId);
  }
}
