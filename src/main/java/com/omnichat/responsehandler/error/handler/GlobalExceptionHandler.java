package com.omnichat.responsehandler.error.handler;

import com.omnichat.common.enums.system.LogKey;
import com.omnichat.responsehandler.error.dto.ErrorResponse;
import com.omnichat.responsehandler.error.enums.DefaultErrorCode;
import com.omnichat.responsehandler.error.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

@RestControllerAdvice
public class GlobalExceptionHandler {
  
  private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

  @ExceptionHandler(Exception.class)
  @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
  public ErrorResponse handleException(Exception exception) {
    logger.error(exception.getMessage(), exception);
    
    String traceId = "";
    var attributes = RequestContextHolder.getRequestAttributes();
    if (attributes != null) {
      traceId = (String) attributes.getAttribute(LogKey.TRACE_ID, RequestAttributes.SCOPE_REQUEST);
    }
    
    if (exception instanceof ServiceException e) {
      return new ErrorResponse(e.getErrorCode(), traceId);
    }

    return new ErrorResponse(DefaultErrorCode.SERVER_ERROR, LogKey.TRACE_ID);
  }
}
