package com.omnichat.responsehandler.error.dto;

public class Detail {
  private final String code;
  private final String message;
  private final String field;
  private final String value;

  public Detail(String code, String message, String field, String value) {
    this.code = code;
    this.message = message;
    this.field = field;
    this.value = value;
  }

  public String getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }

  public String getField() {
    return field;
  }

  public String getValue() {
    return value;
  }
}
