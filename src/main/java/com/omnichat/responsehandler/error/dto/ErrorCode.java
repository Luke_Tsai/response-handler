package com.omnichat.responsehandler.error.dto;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class ErrorCode {
  private final String category;
  private final String code;
  private final String message;
  private List<Detail> details;

  public ErrorCode(String category, String code, String message) {
    this.category = Objects.requireNonNull(category);
    this.code = Objects.requireNonNull(code);
    this.message = Objects.requireNonNull(message);
    details = Collections.emptyList();
  }

  public ErrorCode(String category, String code, String message, List<Detail> details) {
    this(category, code, message);
    this.details = Objects.requireNonNull(details);
  }

  public String getCategory() {
    return category;
  }

  public String getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }

  public List<Detail> getDetails() {
    return details;
  }

  public void setDetails(List<Detail> details) {
    this.details = details == null ? Collections.emptyList() : details;
  }
}
