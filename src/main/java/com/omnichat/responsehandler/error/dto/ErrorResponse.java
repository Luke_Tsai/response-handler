package com.omnichat.responsehandler.error.dto;

import java.util.Objects;

public record ErrorResponse(ErrorCode error, String traceId) {

  public ErrorResponse {
     Objects.requireNonNull(error);
     Objects.requireNonNull(traceId);
  }
}
