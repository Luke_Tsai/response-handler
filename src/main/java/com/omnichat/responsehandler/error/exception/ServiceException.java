package com.omnichat.responsehandler.error.exception;

import com.omnichat.responsehandler.error.dto.ErrorCode;

public class ServiceException extends RuntimeException {

  private final ErrorCode errorCode;

  public ServiceException(ErrorCode errorCode) {
    this.errorCode = errorCode;
  }

  public ErrorCode getErrorCode() {
    return errorCode;
  }
}
