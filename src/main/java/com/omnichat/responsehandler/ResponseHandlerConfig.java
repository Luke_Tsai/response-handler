package com.omnichat.responsehandler;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan
@Configuration
public class ResponseHandlerConfig {}
